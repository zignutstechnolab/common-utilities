/**
 * Final Version
 */


'use strict'

const AWS = require('aws-sdk');

AWS.config.update({region : 'ap-south-1'});

exports.handler = async (event,context) => {

   console.log('********starts Lambda yupeee ********');
   const dynamodb = new AWS.DynamoDB({apiVersion : '2012-08-10'});
   const documentClient = new  AWS.DynamoDB.DocumentClient({region : 'ap-south-1'});

   const params = {
       TableName : 'errorlog',
       // Key : {
       //     id: "1"
       // }
   }

   try{
       const receivedData = await documentClient.scan(params).promise();
       console.log('inside Data');
       console.log(receivedData);
       return receivedData;

   }catch(err){
       console.log('inside catch');
       console.log(err);
       return err;
   }
}




/******************************************
 * This Is Third Version of Lambada get Items From DynamoDb Table and Loging Out Using
 * Json Format that We usually Uesd using async await
 * 
 * *****************************************/

//  'use strict'

//  const AWS = require('aws-sdk');

//  AWS.config.update({region : 'ap-south-1'});

//  exports.handler = async (event,context) => {

//     console.log('********starts Lambda yupeee ********');
//     const dynamodb = new AWS.DynamoDB({apiVersion : '2012-08-10'});
//     const documentClient = new  AWS.DynamoDB.DocumentClient({region : 'ap-south-1'});

//     const params = {
//         TableName : 'errorlog',
//         // Key : {
//         //     id: "1"
//         // }
//     }

//     try{
//     const receivedData = await documentClient.scan(params).promise();
//     console.log('inside Data');
//     console.log(receivedData);

//     }catch(err){
//         console.log('inside catch');
//         console.log(err);
//     }
//  }


 




/******************************************
 * This Is second Version of Lambada get Items From DynamoDb Table and Loging Out Using
 * Json Format that We usually Uesd 
 * 
 * *****************************************/

//  'use strict'

//  const AWS = require('aws-sdk');

//  AWS.config.update({region :'us-east-2'});

//  exports.handler = function(event,context,callback){
//      //code starts

//      console.log('****Starts****');

//      const dynamodB = new AWS.DynamoDB({apiVersion : "2012-08-10"});
//      const documentClient = new AWS.DynamoDB.DocumentClient({region : 'us-east-2'});

//      const params = {
//          TableName : "error_log",
//          Key : {
//              id: "1"
//          },
//      }

//      documentClient.get(params,(err,data) => {
//          if(err){
//              console.log(err);
//          }
//          else{
//              console.log(data);
//          }
//      })
//  }
 


/******************************************
 * This Is First Version of Lambada get Items From DynamoDb Table and Loging Out Using
 * Json Format that dynamo db supports
 * 
 * *****************************************/
 

// 'use strict'

// const AWS = require('aws-sdk');

// AWS.config.update({ region : 'us-east-2'})

// exports.handler = function(event,context,callback){
//     console.log(JSON.stringify(`Event : ${event}`));
//     console.log('inside Handler')

//     var dynamodb = new AWS.DynamoDB();
//     console.log({dynamodb});

//     var params = {
//         Key: {
//          "id": {
//            S: "1"
//           },
//         },
//         TableName: "error_log"
//        };

//     dynamodb.getItem(params, function (err, data) {
//         if (err) {
//             console.log(err, err.stack); // an error occurred
//         }
//         else{
//             console.log(data);           // successful response
//         }     
//     });
// }



/******************************************
 * This is Documetation code of dynamo getItem
 * 
 * *****************************************/

// /* This example retrieves an item from the Music table. The table has a partition key and a sort key (Artist and SongTitle), so you must specify both of these attributes. */

// var params = {
//     Key: {
//      "Artist": {
//        S: "Acme Band"
//       }, 
//      "SongTitle": {
//        S: "Happy Day"
//       }
//     }, 
//     TableName: "Music"
//    };
//    dynamodb.getItem(params, function(err, data) {
//      if (err) console.log(err, err.stack); // an error occurred
//      else     console.log(data);           // successful response
//      /*
//      data = {
//       Item: {
//        "AlbumTitle": {
//          S: "Songs About Life"
//         }, 
//        "Artist": {
//          S: "Acme Band"
//         }, 
//        "SongTitle": {
//          S: "Happy Day"
//         }
//       }
//      }
//      */
//    });


// exports.handler = async (event) => {
//     // TODO implement
//     const response = {
//         statusCode: 200,
//         body: JSON.stringify('Hello from Lambda!'),
//     };
//     return response;
// };
