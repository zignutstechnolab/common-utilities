/**
 * *****************Final Version*******************
 */
/***************************
 * Put Item Final
 ****************************/

'use strict'

const AWS = require('aws-sdk');

AWS.config.update({region : 'ap-south-1'});
let id = 0;

exports.handler = async (event,context) => {
    
    let response = {
        status : '',
        data : '',
        error : ''
    }
    
    console.log(`id: ${id++}`)
    console.log('putErrorLogIntoDynamoD starts code');
    console.log('event');
    console.log(event); 
    
    if(!event.error_code){
        response.status = 400;
        response.error = 'Error Code is Required';
        return response;
    }
    
    if(!event.error_message){
        response.status = 400;
        response.error = 'Error Message is Required';
        return response;
    }
    
    if(!event.details){
        response.status = 400;
        response.error = 'Details About Error is Required';
        return response;
    }
    
    if(!event.project_name){
        response.status = 400;
        response.error = 'Project Name is Required';
        return response;
    }
    
    if(!event.sub_project_name){
        response.status = 400;
        response.error = 'Sub Project Name is Required';
        return response;
    }
    
    if(!event.file_name){
        response.status = 400;
        response.error = 'File Name is Required';
        return response;
    }
    
    if(!event.function_name){
        response.status = 400;
        response.error = 'Function Name is Required';
        return response;
    }
    
    if(!event.version_no){
        response.status = 400;
        response.error = 'Version Number is Required';
        return response;
    }
    
    if(!event.build_no){
        response.status = 400;
        response.error = 'Build Number is Required';
        return response;
    }
    
    if(typeof JSON.parse(event.mail) !== "boolean"){
        response.status = 400;
        response.error = 'Mail Must Required and Bollean';
        return response;
    }

    const dynamoDB = new AWS.DynamoDB({apiVersion : '2012-08-10'});
    
    const documentClient = new AWS.DynamoDB.DocumentClient({region : 'ap-south-1'});

    const params = {
        TableName : 'errorlog',
        Item : {
            id : id.toString(),
            error_message : event.error_message,
            error_code : event.error_code,
            details : event.details,
            project_name : event.project_name,
            sub_project_name : event.sub_project_name,
            file_name : event.file_name,
            function_name : event.function_name,
            mail:JSON.parse(event.mail),
            version_no: event.version_no,
            build_no : event.build_no,
        }
    }
    
    if(JSON.parse(event.mail)){
        if(!event.whom){
            response.status = 400;
            response.error = 'Mail To Whom List Please provide In Comma Separated Mannner.'
            return response;
        }
        
        let names = event.whom;
        let nameArr = names.split(',');
        
        console.log(nameArr);
        
        for(let i =0;i<nameArr.length;i++){
            if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(nameArr[i]))) {
                response.status = 400;
                response.error = 'Invalid email address!';
                return response;                
            }
        }
        
        params.Item.whom = nameArr;
    }

    try{
        const data = await documentClient.put(params).promise();
        console.log('success Put');
        console.log(data);
        
        response.status = 200;
        response.data = data;
        return response;

    }catch(err){
        console.log('inside Catch');
        console.log(err);
        
        response.status = 400;
        response.error = err;
        return response;
    }
}







// /***************************
//  * Put Item
//  */

// 'use strict'

// const AWS = require('aws-sdk');

// AWS.config.update({region : 'ap-south-1'});

// exports.handler = async (event,context) => {
//     console.log('Yupee starts code');

//     const dynamoDB = new AWS.DynamoDB({apiVersion : '2012-08-10'});
    
//     const documentClient = new AWS.DynamoDB.DocumentClient({region : 'ap-south-1'});

//     const params = {
//         TableName : 'errorlog',
//         Item : {
//             id : "1",
//             error_message : 'Error 1',
//             error_code : 400
//         }
//     }

//     try{
//         const data = await documentClient.put(params).promise();
//         console.log('success Put');
//         console.log(data);

//     }catch(err){
//         console.log('inside Catch');
//         console.log(err);
//     }
// }